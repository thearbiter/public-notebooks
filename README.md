# Public!
This repository is public to allow accessing jupiter notebooks e.g. via https://mybinder.org/ .

[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/thearbiter%2Fpublic-notebooks/master?filepath=transfer_N.ipynb)

## Edit
Changes to the notebook cannot be committed from mybinder back to this repository.

Workaround: Download notebook (*.ipynb) and replace this in the repo.